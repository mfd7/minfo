import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:http/http.dart' as http;
import 'package:minfo/models/movie_details_model.dart';
import 'package:minfo/models/movie_model.dart';
import 'package:minfo/models/now_playing_model.dart';

class Api {
  static var client = http.Client();

  static Future<List<Results>?> fetchMovies() async {
    var response = await client.get(Uri.parse(
        'https://api.themoviedb.org/3/trending/movie/week?api_key=33a0bcbd1c451c1e4b4ee2e4cbb357f5'));
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return resultsFromJson(jsonString);
    } else {
      //show error message
      return null;
    }
  }

  static Future<List<NowPlayingResults>?> fetchNowPlaying() async {
    var response = await client.get(Uri.parse(
        'https://api.themoviedb.org/3/movie/now_playing?api_key=33a0bcbd1c451c1e4b4ee2e4cbb357f5&language=en-US&page=1'));
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return nowPlayingResultsFromJson(jsonString);
    } else {
      //show error message
      return null;
    }
  }

  static Future<List<Results>?> fetchByGenres(RxString id) async {
    var response = await client.get(Uri.parse(
        'https://api.themoviedb.org/3/discover/movie?api_key=33a0bcbd1c451c1e4b4ee2e4cbb357f5&language=en-US&page=1&primary_release_date.gte=2020-12-30&with_genres=${id}'));
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return resultsFromJson(jsonString);
    } else {
      //show error message
      return null;
    }
  }

  static Future<String?> fetchPosterPath(movieId) async {
    var response = await client.get(Uri.parse(
        'https://api.themoviedb.org/3/movie/${movieId}?api_key=33a0bcbd1c451c1e4b4ee2e4cbb357f5&append_to_response=videos'));
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return moviePosterPathFromJson(jsonString);
    } else {
      //show error message
      return null;
    }
  }

  static Future<String?> fetchTitle(movieId) async {
    var response = await client.get(Uri.parse(
        'https://api.themoviedb.org/3/movie/${movieId}?api_key=33a0bcbd1c451c1e4b4ee2e4cbb357f5&append_to_response=videos'));
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return movieTitleFromJson(jsonString);
    } else {
      //show error message
      return null;
    }
  }

  static Future<String?> fetchVideo(movieId) async {
    var response = await client.get(Uri.parse(
        'https://api.themoviedb.org/3/movie/${movieId}?api_key=33a0bcbd1c451c1e4b4ee2e4cbb357f5&append_to_response=videos'));
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return movieVideoFromJson(jsonString);
    } else {
      //show error message
      return null;
    }
  }

  static Future<String?> fetchDesc(movieId) async {
    var response = await client.get(Uri.parse(
        'https://api.themoviedb.org/3/movie/${movieId}?api_key=33a0bcbd1c451c1e4b4ee2e4cbb357f5&append_to_response=videos'));
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return movieDescFromJson(jsonString);
    } else {
      //show error message
      return null;
    }
  }

  static Future<String?> fetchRelease(movieId) async {
    var response = await client.get(Uri.parse(
        'https://api.themoviedb.org/3/movie/${movieId}?api_key=33a0bcbd1c451c1e4b4ee2e4cbb357f5&append_to_response=videos'));
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return movieReleaseFromJson(jsonString);
    } else {
      //show error message
      return null;
    }
  }
}

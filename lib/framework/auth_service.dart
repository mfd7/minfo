import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:minfo/pages/logged/page_home_screen.dart';
import 'package:minfo/pages/non_logged/page_login_screen.dart';
import 'package:minfo/shared/constants/colors.dart';

class AuthService {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<void> signIn({required String email, required String password}) async {
    try {
      await _firebaseAuth
          .signInWithEmailAndPassword(email: email, password: password)
          .then((value) => Get.to(() => HomeScreen()));
    } on FirebaseAuthException catch (e) {
      Get.snackbar("Login Failed", e.message!,
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: primary,
          colorText: Colors.white);
    }
  }

  void signUp({required String email, required String password}) async {
    try {
      await _firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((value) => Get.to(() => LoginScreen()));
      Get.snackbar("Register Success", "Please login to continue",
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: primary,
          colorText: Colors.white);
    } on FirebaseAuthException catch (e) {
      Get.snackbar("Register Failed", e.message!,
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: primary,
          colorText: Colors.white);
    }
  }

  Future<void> signOut() async {
    await _firebaseAuth.signOut().then((value) => Get.to(() => LoginScreen()));
  }

  String? currentEmail() {
    return _firebaseAuth.currentUser!.email;
  }
}

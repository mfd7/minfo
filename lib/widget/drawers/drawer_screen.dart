import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:minfo/framework/auth_service.dart';
import 'package:minfo/pages/logged/page_about_screen.dart';
import 'package:minfo/shared/constants/colors.dart';

class DrawerScreen extends StatelessWidget {
  AuthService authService = AuthService();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              children: <Widget>[
                UserAccountsDrawerHeader(
                  accountName: Text("Mohammad Faishal Dzaky"),
                  currentAccountPicture: CircleAvatar(
                    backgroundImage: AssetImage("assets/img/foto.jpg"),
                  ),
                  accountEmail: Text(authService.currentEmail()!),
                  decoration: BoxDecoration(color: primary),
                ),
                DrawerListTitle(
                  iconData: Icons.info,
                  title: "About Me",
                  onTilePressed: () {
                    Get.to(() => AboutScreen());
                  },
                ),
              ],
            ),
          ),
          Container(
              child: Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: Container(
                      child: Column(
                    children: <Widget>[
                      Divider(),
                      DrawerListTitle(
                        iconData: Icons.power_settings_new,
                        title: "Log Out",
                        onTilePressed: () {
                          authService.signOut();
                        },
                      ),
                    ],
                  ))))
        ],
      ),
    );
  }
}

class DrawerListTitle extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTitle(
      {Key? key,
      required this.iconData,
      required this.title,
      required this.onTilePressed})
      : super(key: key);

  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}

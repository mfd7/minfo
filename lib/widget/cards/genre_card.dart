import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:minfo/controller/movie_details_controller.dart';
import 'package:minfo/models/movie_model.dart';
import 'package:minfo/pages/logged/page_movie_details_screen.dart';

class GenreCard extends StatelessWidget {
  final Results movies;
  GenreCard(this.movies);
  final MovieDetailsController movieDetailsController =
      Get.put(MovieDetailsController());

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () async {
          movieDetailsController.fetchMovieDetails(movies.id);
          Get.to(() => MovieDetails());
        },
        child: Card(
          elevation: 2,
          child: SizedBox(
            width: 200,
            height: 260,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    if (movies.posterPath != null)
                      Container(
                        height: 260,
                        width: 200,
                        clipBehavior: Clip.antiAlias,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: Image.network(
                          'https://image.tmdb.org/t/p/original/${movies.posterPath}',
                          fit: BoxFit.fitHeight,
                        ),
                      )
                    else
                      Container(
                        height: 260,
                        width: 200,
                        clipBehavior: Clip.antiAlias,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: Image.network(
                          'https://user-images.githubusercontent.com/24848110/33519396-7e56363c-d79d-11e7-969b-09782f5ccbab.png',
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                    Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        color: Colors.black.withOpacity(0.5),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Center(
                                child: Text(
                                  '${movies.title} (${movies.releaseDate?.substring(0, 4)})',
                                  style: TextStyle(
                                      fontFamily: 'avenir',
                                      fontWeight: FontWeight.w800,
                                      color: Colors.white),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    if (movies.id != null)
                      Positioned(
                        top: 0,
                        right: 0,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(12),
                          ),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 4, vertical: 2),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                movies.voteAverage.toString(),
                                style: TextStyle(color: Colors.white),
                              ),
                              Icon(
                                Icons.star,
                                size: 16,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

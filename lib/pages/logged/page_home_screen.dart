import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:minfo/controller/movie_controller.dart';
import 'package:minfo/controller/movie_details_controller.dart';
import 'package:minfo/shared/constants/colors.dart';
import 'package:minfo/widget/cards/genre_card.dart';
import 'package:minfo/widget/cards/movie_card.dart';
import 'package:minfo/widget/cards/now_playing_card.dart';
import 'package:minfo/widget/drawers/drawer_screen.dart';

class HomeScreen extends StatelessWidget {
  final MovieController movieController = Get.put(MovieController());
  final MovieDetailsController movieDetailsController =
      Get.put(MovieDetailsController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
        backgroundColor: primary,
        centerTitle: true,
      ),
      drawer: DrawerScreen(),
      body: Column(
        children: [
          Expanded(
            child: Obx(() {
              if (movieController.isLoading.value)
                return Center(child: CircularProgressIndicator());
              else
                return ListView(
                  children: [
                    Column(
                      children: [
                        Container(
                          color: Colors.white,
                          child: ListView(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              children: [
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "Trending Movies",
                                      style: TextStyle(
                                          fontFamily: 'avenir',
                                          fontWeight: FontWeight.w800,
                                          fontSize: 20,
                                          color: Colors.black),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 188,
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    physics: ClampingScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount: movieController.movieList.length,
                                    itemBuilder: (context, index) {
                                      return MovieCard(
                                          movieController.movieList[index]);
                                    },
                                  ),
                                ),
                              ]),
                        ),
                        SizedBox(
                          height: 20,
                          child: Container(
                            color: Colors.white,
                          ),
                        ),
                        Container(
                          color: Colors.white,
                          child: ListView(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              children: [
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "Now Playing",
                                      style: TextStyle(
                                          fontFamily: 'avenir',
                                          fontWeight: FontWeight.w800,
                                          fontSize: 20,
                                          color: Colors.black),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 188,
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    physics: ClampingScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount:
                                        movieController.nowPlayingList.length,
                                    itemBuilder: (context, index) {
                                      return NowPlayingCard(movieController
                                          .nowPlayingList[index]);
                                    },
                                  ),
                                ),
                              ]),
                        ),
                        SizedBox(
                          height: 20,
                          child: Container(
                            color: Colors.white,
                          ),
                        ),
                        Container(
                          color: Colors.white,
                          width: double.infinity,
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "Search by Genres",
                                style: TextStyle(
                                    fontFamily: 'avenir',
                                    fontWeight: FontWeight.w800,
                                    fontSize: 20,
                                    color: Colors.black),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          color: Colors.white,
                          child: SizedBox(
                            height: 100,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              physics: ClampingScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: movieController.chipList.length,
                              itemBuilder: (context, index) {
                                return Padding(
                                    padding: const EdgeInsets.only(
                                        left: 10, right: 5),
                                    child: FilterChip(
                                      label: Text(movieController
                                          .chipList[index].label),
                                      labelStyle:
                                          TextStyle(color: Colors.white),
                                      backgroundColor:
                                          movieController.chipList[index].color,
                                      selected: movieController
                                          .chipList[index].isSelected,
                                      onSelected: (bool value) {
                                        if (movieController
                                                .chipList[index].isSelected ==
                                            true) {
                                          movieController.chipList[index]
                                              .isSelected = false;
                                          movieController.listId.remove(
                                              '${movieController.chipList[index].id}');
                                        } else {
                                          movieController.chipList[index]
                                              .isSelected = true;
                                          movieController.listId.add(
                                              movieController.chipList[index].id
                                                  .toString());
                                        }

                                        movieController.chipList.refresh();
                                        movieController.listId.refresh();
                                        movieController.id = movieController
                                            .listId
                                            .join(',')
                                            .obs;
                                        movieController
                                            .fetchByGenres(movieController.id);
                                      },
                                    ));
                              },
                            ),
                          ),
                        ),
                        if (movieController.isGenreLoading.value)
                          Center(child: CircularProgressIndicator())
                        else
                          Container(
                            color: Colors.white,
                            child: StaggeredGridView.countBuilder(
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              crossAxisCount: 2,
                              itemCount: movieController.movieByGenres.length,
                              itemBuilder: (context, index) {
                                return Center(
                                    child: GenreCard(
                                        movieController.movieByGenres[index]));
                              },
                              staggeredTileBuilder: (index) =>
                                  StaggeredTile.fit(1),
                            ),
                          )
                      ],
                    ),
                  ],
                );
            }),
          )
        ],
      ),
    );
  }
}

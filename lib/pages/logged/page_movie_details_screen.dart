import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:get/get.dart';
import 'package:minfo/controller/movie_details_controller.dart';
import 'package:get/state_manager.dart';
import 'package:minfo/shared/constants/colors.dart';

class MovieDetails extends StatelessWidget {
  final MovieDetailsController movieDetailsController =
      Get.put(MovieDetailsController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Movie Details"),
        backgroundColor: primary,
        centerTitle: true,
      ),
      body: Obx(
        () {
          if (movieDetailsController.isLoading.value)
            return Center(child: CircularProgressIndicator());
          else
            return ListView(
              children: [
                Center(
                  child: Image.network(
                    'https://image.tmdb.org/t/p/original${movieDetailsController.moviePosterPath}',
                    fit: BoxFit.fitHeight,
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                        child: Text(
                      '${movieDetailsController.movieTitle} (${movieDetailsController.movieRelease.substring(0, 4)})',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'avenir',
                          fontWeight: FontWeight.w800,
                          fontSize: 30),
                    )),
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Center(
                        child: Text(
                      movieDetailsController.movieDesc.value,
                      textAlign: TextAlign.justify,
                    )),
                  ),
                ),
                SizedBox(
                  height: 20,
                  child: Container(
                    color: Colors.white,
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Trailer",
                      style: TextStyle(
                          fontFamily: 'avenir',
                          fontWeight: FontWeight.w800,
                          fontSize: 20,
                          color: Colors.black),
                    ),
                  ),
                ),
                HtmlWidget(
                  '<iframe src="https://www.youtube.com/embed/${movieDetailsController.movieVideo.value}"></iframe>',
                  webView: true,
                ),
                SizedBox(
                  height: 20,
                  child: Container(
                    color: Colors.white,
                  ),
                ),
              ],
            );
        },
      ),
    );
  }
}

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:minfo/framework/auth_service.dart';
import 'package:minfo/shared/constants/assets.dart';
import 'package:minfo/shared/constants/colors.dart';

class RegisterScreen extends StatelessWidget {
  AuthService authService = AuthService();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text("REGISTER"),
        backgroundColor: primary,
        centerTitle: true,
      ),
      body: ListView(children: [
        const Image(
          image: login,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                  top: 10, right: 20, bottom: 10, left: 20),
              child: TextFormField(
                decoration: const InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: primary,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: primary,
                      ),
                    ),
                    hintText: "Email.."),
                controller: _emailController,
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 10, right: 20, bottom: 10, left: 20),
              child: TextFormField(
                decoration: const InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: primary,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: primary,
                      ),
                    ),
                    hintText: "Password.."),
                controller: _passwordController,
                textAlign: TextAlign.center,
                obscureText: true,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 10, right: 20, bottom: 10, left: 20),
              child: TextFormField(
                decoration: const InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: primary,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: primary,
                      ),
                    ),
                    hintText: "Confirm Password.."),
                controller: _confirmPasswordController,
                textAlign: TextAlign.center,
                obscureText: true,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 50, right: 20, bottom: 20, left: 20),
              child: SizedBox(
                width: double.infinity,
                height: 50,
                child: ElevatedButton(
                  onPressed: () {
                    if (_passwordController.text !=
                        _confirmPasswordController.text) {
                      Get.snackbar(
                          "Login Failed", "Please check your password!",
                          snackPosition: SnackPosition.BOTTOM,
                          backgroundColor: primary,
                          colorText: Colors.white);
                    } else {
                      authService.signUp(
                          email: _emailController.text,
                          password: _passwordController.text);
                    }
                  },
                  child: const Text(
                    "REGISTER",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: buttons,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ]),
    );
  }
}

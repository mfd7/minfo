import 'dart:convert';
import 'package:get/get.dart';

List<Results> resultsFromJson(String str) => List<Results>.from(
    (json.decode(str)['results'] as List).map((x) => Results.fromJson(x)));

String productToJson(List<Movies> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Movies {
  int? page;
  List<Results>? results;
  int? totalPages;
  int? totalResults;

  Movies({this.page, this.results, this.totalPages, this.totalResults});

  Movies.fromJson(Map<String, dynamic> json) {
    if (json["page"] is int) this.page = json["page"];
    if (json["results"] is List)
      this.results = json["results"] == null
          ? null
          : (json["results"] as List).map((e) => Results.fromJson(e)).toList();
    if (json["total_pages"] is int) this.totalPages = json["total_pages"];
    if (json["total_results"] is int) this.totalResults = json["total_results"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["page"] = this.page;
    if (this.results != null)
      data["results"] = this.results?.map((e) => e.toJson()).toList();
    data["total_pages"] = this.totalPages;
    data["total_results"] = this.totalResults;
    return data;
  }
}

class Results {
  List<int>? genreIds;
  String? originalLanguage;
  String? originalTitle;
  String? posterPath;
  bool? video;
  double? voteAverage;
  String? title;
  String? overview;
  String? releaseDate;
  int? voteCount;
  bool? adult;
  String? backdropPath;
  int? id;
  double? popularity;
  String? mediaType;

  Results(
      {this.genreIds,
      this.originalLanguage,
      this.originalTitle,
      this.posterPath,
      this.video,
      this.voteAverage,
      this.title,
      this.overview,
      this.releaseDate,
      this.voteCount,
      this.adult,
      this.backdropPath,
      this.id,
      this.popularity,
      this.mediaType});

  Results.fromJson(Map<String, dynamic> json) {
    if (json["genre_ids"] is List)
      this.genreIds =
          json["genre_ids"] == null ? null : List<int>.from(json["genre_ids"]);
    if (json["original_language"] is String)
      this.originalLanguage = json["original_language"];
    if (json["original_title"] is String)
      this.originalTitle = json["original_title"];
    if (json["poster_path"] is String) this.posterPath = json["poster_path"];
    if (json["video"] is bool) this.video = json["video"];
    if (json["vote_average"] is double) this.voteAverage = json["vote_average"];
    if (json["title"] is String) this.title = json["title"];
    if (json["overview"] is String) this.overview = json["overview"];
    if (json["release_date"] is String) this.releaseDate = json["release_date"];
    if (json["vote_count"] is int) this.voteCount = json["vote_count"];
    if (json["adult"] is bool) this.adult = json["adult"];
    if (json["backdrop_path"] is String)
      this.backdropPath = json["backdrop_path"];
    if (json["id"] is int) this.id = json["id"];
    if (json["popularity"] is double) this.popularity = json["popularity"];
    if (json["media_type"] is String) this.mediaType = json["media_type"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.genreIds != null) data["genre_ids"] = this.genreIds;
    data["original_language"] = this.originalLanguage;
    data["original_title"] = this.originalTitle;
    data["poster_path"] = this.posterPath;
    data["video"] = this.video;
    data["vote_average"] = this.voteAverage;
    data["title"] = this.title;
    data["overview"] = this.overview;
    data["release_date"] = this.releaseDate;
    data["vote_count"] = this.voteCount;
    data["adult"] = this.adult;
    data["backdrop_path"] = this.backdropPath;
    data["id"] = this.id;
    data["popularity"] = this.popularity;
    data["media_type"] = this.mediaType;
    return data;
  }
}

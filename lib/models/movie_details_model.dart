import 'dart:convert';

import 'package:get/get.dart';

String moviePosterPathFromJson(String str) => json.decode(str)['poster_path'];
String movieTitleFromJson(String str) => json.decode(str)['original_title'];
String movieVideoFromJson(String str) =>
    json.decode(str)['videos']['results'][0]['key'];
String movieDescFromJson(String str) => json.decode(str)['overview'];
String movieReleaseFromJson(String str) => json.decode(str)['release_date'];

class MovieDetailsModel {
  bool? adult;
  String? backdropPath;
  dynamic? belongsToCollection;
  int? budget;
  List<Genres>? genres;
  String? homepage;
  RxInt? id;
  String? imdbId;
  String? originalLanguage;
  String? originalTitle;
  String? overview;
  double? popularity;
  String? posterPath;
  List<ProductionCountries>? productionCountries;
  String? releaseDate;
  int? revenue;
  int? runtime;
  List<SpokenLanguages>? spokenLanguages;
  String? status;
  String? tagline;
  String? title;
  bool? video;
  double? voteAverage;
  int? voteCount;
  Videos? videos;

  MovieDetailsModel(
      {this.adult,
      this.backdropPath,
      this.belongsToCollection,
      this.budget,
      this.genres,
      this.homepage,
      this.id,
      this.imdbId,
      this.originalLanguage,
      this.originalTitle,
      this.overview,
      this.popularity,
      this.posterPath,
      this.productionCountries,
      this.releaseDate,
      this.revenue,
      this.runtime,
      this.spokenLanguages,
      this.status,
      this.tagline,
      this.title,
      this.video,
      this.voteAverage,
      this.voteCount,
      this.videos});

  MovieDetailsModel.fromJson(Map<String, dynamic> json) {
    if (json["adult"] is bool) this.adult = json["adult"];
    if (json["backdrop_path"] is String)
      this.backdropPath = json["backdrop_path"];
    this.belongsToCollection = json["belongs_to_collection"];
    if (json["budget"] is int) this.budget = json["budget"];
    if (json["genres"] is List)
      this.genres = json["genres"] == null
          ? null
          : (json["genres"] as List).map((e) => Genres.fromJson(e)).toList();
    if (json["homepage"] is String) this.homepage = json["homepage"];
    if (json["id"] is int) this.id = json["id"];
    if (json["imdb_id"] is String) this.imdbId = json["imdb_id"];
    if (json["original_language"] is String)
      this.originalLanguage = json["original_language"];
    if (json["original_title"] is String)
      this.originalTitle = json["original_title"];
    if (json["overview"] is String) this.overview = json["overview"];
    if (json["popularity"] is double) this.popularity = json["popularity"];
    if (json["poster_path"] is String) this.posterPath = json["poster_path"];
    if (json["production_countries"] is List)
      this.productionCountries = json["production_countries"] == null
          ? null
          : (json["production_countries"] as List)
              .map((e) => ProductionCountries.fromJson(e))
              .toList();
    if (json["release_date"] is String) this.releaseDate = json["release_date"];
    if (json["revenue"] is int) this.revenue = json["revenue"];
    if (json["runtime"] is int) this.runtime = json["runtime"];
    if (json["spoken_languages"] is List)
      this.spokenLanguages = json["spoken_languages"] == null
          ? null
          : (json["spoken_languages"] as List)
              .map((e) => SpokenLanguages.fromJson(e))
              .toList();
    if (json["status"] is String) this.status = json["status"];
    if (json["tagline"] is String) this.tagline = json["tagline"];
    if (json["title"] is String) this.title = json["title"];
    if (json["video"] is bool) this.video = json["video"];
    if (json["vote_average"] is double) this.voteAverage = json["vote_average"];
    if (json["vote_count"] is int) this.voteCount = json["vote_count"];
    if (json["videos"] is Map)
      this.videos =
          json["videos"] == null ? null : Videos.fromJson(json["videos"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["adult"] = this.adult;
    data["backdrop_path"] = this.backdropPath;
    data["belongs_to_collection"] = this.belongsToCollection;
    data["budget"] = this.budget;
    if (this.genres != null)
      data["genres"] = this.genres?.map((e) => e.toJson()).toList();
    data["homepage"] = this.homepage;
    data["id"] = this.id;
    data["imdb_id"] = this.imdbId;
    data["original_language"] = this.originalLanguage;
    data["original_title"] = this.originalTitle;
    data["overview"] = this.overview;
    data["popularity"] = this.popularity;
    data["poster_path"] = this.posterPath;
    if (this.productionCountries != null)
      data["production_countries"] =
          this.productionCountries?.map((e) => e.toJson()).toList();
    data["release_date"] = this.releaseDate;
    data["revenue"] = this.revenue;
    data["runtime"] = this.runtime;
    if (this.spokenLanguages != null)
      data["spoken_languages"] =
          this.spokenLanguages?.map((e) => e.toJson()).toList();
    data["status"] = this.status;
    data["tagline"] = this.tagline;
    data["title"] = this.title;
    data["video"] = this.video;
    data["vote_average"] = this.voteAverage;
    data["vote_count"] = this.voteCount;
    if (this.videos != null) data["videos"] = this.videos?.toJson();
    return data;
  }
}

class Videos {
  List<VideoResults>? results;

  Videos({this.results});

  Videos.fromJson(Map<String, dynamic> json) {
    if (json["results"] is List)
      this.results = json["results"] == null
          ? null
          : (json["results"] as List)
              .map((e) => VideoResults.fromJson(e))
              .toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.results != null)
      data["results"] = this.results?.map((e) => e.toJson()).toList();
    return data;
  }
}

class VideoResults {
  String? iso6391;
  String? iso31661;
  String? name;
  String? key;
  String? site;
  int? size;
  String? type;
  bool? official;
  String? publishedAt;
  String? id;

  VideoResults(
      {this.iso6391,
      this.iso31661,
      this.name,
      this.key,
      this.site,
      this.size,
      this.type,
      this.official,
      this.publishedAt,
      this.id});

  VideoResults.fromJson(Map<String, dynamic> json) {
    if (json["iso_639_1"] is String) this.iso6391 = json["iso_639_1"];
    if (json["iso_3166_1"] is String) this.iso31661 = json["iso_3166_1"];
    if (json["name"] is String) this.name = json["name"];
    if (json["key"] is String) this.key = json["key"];
    if (json["site"] is String) this.site = json["site"];
    if (json["size"] is int) this.size = json["size"];
    if (json["type"] is String) this.type = json["type"];
    if (json["official"] is bool) this.official = json["official"];
    if (json["published_at"] is String) this.publishedAt = json["published_at"];
    if (json["id"] is String) this.id = json["id"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["iso_639_1"] = this.iso6391;
    data["iso_3166_1"] = this.iso31661;
    data["name"] = this.name;
    data["key"] = this.key;
    data["site"] = this.site;
    data["size"] = this.size;
    data["type"] = this.type;
    data["official"] = this.official;
    data["published_at"] = this.publishedAt;
    data["id"] = this.id;
    return data;
  }
}

class SpokenLanguages {
  String? englishName;
  String? iso6391;
  String? name;

  SpokenLanguages({this.englishName, this.iso6391, this.name});

  SpokenLanguages.fromJson(Map<String, dynamic> json) {
    if (json["english_name"] is String) this.englishName = json["english_name"];
    if (json["iso_639_1"] is String) this.iso6391 = json["iso_639_1"];
    if (json["name"] is String) this.name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["english_name"] = this.englishName;
    data["iso_639_1"] = this.iso6391;
    data["name"] = this.name;
    return data;
  }
}

class ProductionCountries {
  String? iso31661;
  String? name;

  ProductionCountries({this.iso31661, this.name});

  ProductionCountries.fromJson(Map<String, dynamic> json) {
    if (json["iso_3166_1"] is String) this.iso31661 = json["iso_3166_1"];
    if (json["name"] is String) this.name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["iso_3166_1"] = this.iso31661;
    data["name"] = this.name;
    return data;
  }
}

class Genres {
  int? id;
  String? name;

  Genres({this.id, this.name});

  Genres.fromJson(Map<String, dynamic> json) {
    if (json["id"] is int) this.id = json["id"];
    if (json["name"] is String) this.name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["id"] = this.id;
    data["name"] = this.name;
    return data;
  }
}

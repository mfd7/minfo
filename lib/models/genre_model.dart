import 'package:flutter/material.dart';

class Chips {
  String label;
  Color color;
  bool isSelected;
  int id;
  Chips(this.label, this.color, this.isSelected, this.id);
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:minfo/framework/api.dart';
import 'package:get/state_manager.dart';
import 'package:minfo/models/genre_model.dart';
import 'package:minfo/models/movie_model.dart';
import 'package:minfo/models/now_playing_model.dart';

class MovieController extends GetxController {
  var isLoading = true.obs;
  var isGenreLoading = true.obs;
  var movieList = <Results>[].obs;
  var movieByGenres = <Results>[].obs;
  var nowPlayingList = <NowPlayingResults>[].obs;
  var chipList = <Chips>[
    Chips("Action", Colors.red, false, 28),
    Chips("Adventure", Colors.green, false, 12),
    Chips("Animation", Colors.lightBlue, false, 16),
    Chips("Comedy", Colors.deepOrange, false, 35),
    Chips("Crime", Colors.black, false, 80),
    Chips("Documentary", Colors.grey, false, 99),
    Chips("Drama", Colors.purple, false, 18),
    Chips("Family", Colors.amber, false, 10751),
    Chips("Fantasy", Colors.lime, false, 14),
    Chips("History", Colors.black, false, 36),
    Chips("Horror", Colors.grey, false, 27),
    Chips("Music", Colors.brown, false, 10402),
    Chips("Mystery", Colors.black, false, 9648),
    Chips("Romance", Colors.pink, false, 10749),
    Chips("Sci-fi", Colors.blue, false, 878),
    Chips("Thriller", Colors.black, false, 53),
    Chips("War", Colors.grey, false, 10752),
    Chips("Western", Colors.orange, false, 37),
  ].obs;
  var listId = <String>[].obs;
  var id = ''.obs;

  @override
  void onInit() {
    fetchMovies();
    fetchNowPlaying();
    fetchByGenres(id);
    print(movieList);
    super.onInit();
  }

  void fetchMovies() async {
    try {
      isLoading(true);
      var movies = await Api.fetchMovies();
      if (movies != null) {
        movieList.value = movies;
      }
    } finally {
      isLoading(false);
    }
  }

  void fetchNowPlaying() async {
    try {
      isLoading(true);
      var movies = await Api.fetchNowPlaying();
      if (movies != null) {
        nowPlayingList.value = movies;
      }
    } finally {
      isLoading(false);
    }
  }

  void fetchByGenres(RxString? id) async {
    try {
      isGenreLoading(true);
      var movies = await Api.fetchByGenres(id!);
      if (movies != null) {
        movieByGenres.value = movies;
      }
    } finally {
      isGenreLoading(false);
    }
  }
}

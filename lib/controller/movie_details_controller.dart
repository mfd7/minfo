import 'package:get/get.dart';
import 'package:minfo/framework/api.dart';

class MovieDetailsController extends GetxController {
  var isLoading = true.obs;
  var moviePosterPath = '/yc2IfL701hGkNHRgzmF4C6VKO14.jpg'.obs;
  var movieTitle = ''.obs;
  var movieDesc = ''.obs;
  var movieVideo = ''.obs;
  var movieRelease = ''.obs;
  var movieId = 566525.obs;

  void fetchMovieDetails(movieId) async {
    try {
      isLoading(true);
      var poster = await Api.fetchPosterPath(movieId);
      var title = await Api.fetchTitle(movieId);
      var desc = await Api.fetchDesc(movieId);
      var video = await Api.fetchVideo(movieId);
      var release = await Api.fetchRelease(movieId);
      if (poster != null &&
          title != null &&
          desc != null &&
          video != null &&
          release != null) {
        moviePosterPath.value = poster;
        movieTitle.value = title;
        movieDesc.value = desc;
        movieVideo.value = video;
        movieRelease.value = release;
      }
    } finally {
      isLoading(false);
    }
  }
}
